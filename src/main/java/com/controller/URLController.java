package com.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class URLController {
	
	@Value("${test.url1}")
	private String testUrl1;
	
	@Value("${test.url2}")
	private String testUrl2;
	
	
   // This method is used to map anything which user sent a request
	@GetMapping(value = {
	        "","/*/*",
	        "/page",
	        "page*",
	        "view/*",
	        "**/msg","/test"})
	public String dynamicURL1() {
		return testUrl1;
	}
	
	/*
	 * This method is used to redirect request to the specified URL this is
	 * hiding of our main url where application is deployed
	 */
	@GetMapping(value = {"/test2"})
	public String dynamicURL2() {
		return testUrl2;
	}

}
