package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendlyUrlApplication {

	public static void main(String[] args) {
		SpringApplication.run(FriendlyUrlApplication.class, args);
	}

}
